# 国密算法
	zjl国密标准实现包
	软签名算法实现
## 项目打包
	mvn install #无报错后引入依赖到pom.xml文件
## 使用
### 1. 引入依赖
	<dependency>
      <groupId>com.zjl.common</groupId>
      <artifactId>gm_encryptor</artifactId>
      <version>1.0-SNAPSHOT</version>
	</dependency>
### 2. 安装jar到本地mvn库
	#注意更换-Dfile的路径,需要自定义版本修改-Dversion属性
	mvn install:install-file -Dfile=D:/path/gm_encryptor.jar -DgroupId=com.zjl.common -DartifactId=gm_encryptor -Dversion=1.0-SNAPSHOT -Dpackaging=jar
	
### 修改记录
#### @date 2018/8/21
	添加sk->priKey,pem->PubKey,生成自签名国密证书三个接口函数及实现,功能测试通过,cvm证书链校验通过
#### @date 2018/8/27
	添加sm4加密私钥的自签名证书函数,修改自签名证书函数为允许传参CN生成指定的cnname 测试通过
	

### 建议
	1. 关闭日志使用String打印到控制台,已经有运行时异常,异常消息明确,无需日志
	2. 去除@data标签
	
### 问题
	2. testsm2_std sign 多 00
	