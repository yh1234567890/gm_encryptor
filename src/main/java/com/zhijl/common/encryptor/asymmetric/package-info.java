package com.zhijl.common.encryptor.asymmetric;


/***

 NOTICE by wild on 20180119:

 1. 上一个asymmetric包，在进行签名的时候少了一个的如下运算时候
 杂凑值 ZA = H256(ENTLA || IDA || a || b || xg || yg || xa || ya )
 少一个ENTLA,本次添加这个参数。

 2. 本包的测试用例：
 test.com.zjl.encryptor.asymmetric.TestSM2_STD

 3. 调整代码中的字符拼接操作

 **/