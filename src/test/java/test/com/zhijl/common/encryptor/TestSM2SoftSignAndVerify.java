package test.com.zhijl.common.encryptor;

import com.zhijl.common.encryptor.asymmetric.SM2;
import com.zhijl.common.encryptor.asymmetric.SM2Impl;
import com.zhijl.common.encryptor.asymmetric.SM2KeyPair;
import com.zhijl.common.encryptor.bytetool.Hex;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECPoint;
import org.bouncycastle.openssl.PEMException;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.interfaces.ECPrivateKey;
import java.util.Base64;

import static com.zhijl.common.encryptor.asymmetric.SM2KeyPair.genByHexString;
import static org.junit.Assert.*;

/**
 * 〈sm2 常用测试类〉
 *
 * @author liu
 * @date 2018/6/25
 * @since 1.0.0
 */
public class TestSM2SoftSignAndVerify {

    /**
     * 使用sign的r,s重新生成asn1结构sign数据
     */
    @Test
    public void testGenSigntureFromRS() {

        String expectR = "40F1EC59 F793D9F4 9E09DCEF 49130D41 94F79FB1 EED2CAA5 5BACDB49 C4E755D1".replaceAll(" ", "").toLowerCase();
        String expectS = "6FC6DAC3 2C5D5CF1 0C77DFB2 0F7C2EB6 67A45787 2FB09EC5 6327A67E C7DEEBE7".replaceAll(" ", "").toLowerCase();

        BigInteger r = new BigInteger(Hex.decode(expectR));
        BigInteger s = new BigInteger(Hex.decode(expectS));

        SM2Impl.Signature signature = new SM2Impl.Signature(r, s);

        System.out.println("asn.1 sign:\t" + Hex.encode(signature.encodeASN1()));
        System.out.println("asn.1 sign len:\t" + Hex.encode(signature.encodeASN1()).length());

        assertEquals(expectR, signature.getR());
        assertEquals(expectS, signature.getS());

        String hexSignStr = "3044022040f1ec59f793d9f49e09dcef49130d4194f79fb1eed2caa55bacdb49c4e755d102206fc6dac32c5d5cf10c77dfb20f7c2eb667a457872fb09ec56327a67ec7deebe7";
        SM2Impl.Signature signature2 = new SM2Impl.Signature(Hex.decode(hexSignStr));
        assertArrayEquals(Hex.decode(hexSignStr), signature2.encodeASN1());

        assertEquals(expectR, signature2.getR());
        assertEquals(expectS, signature2.getS());

    }

    /**
     * 通过私钥生成sm2KeyPart
     */
    @Test
    public void testGenKeyPartByPriKey() {

        SM2Impl sm2 = new SM2Impl();
        // asn1 hex priKey data
        String priKey = "3756346e43268df626d8c476bced67a8643319775073057cd6327f8a0ccd68c6";
        BigInteger privateKey = sm2.decodePrivateKey(priKey);

        SM2KeyPair sm2KeyPair = new SM2KeyPair(SM2Impl.getG().multiply(privateKey).normalize(), privateKey);
        System.out.println("pubKey:   " + Hex.encode(sm2KeyPair.getPublicKey().getEncoded()));
        System.out.println("pubKey x: " + Hex.encode(sm2KeyPair.getPublicKey().getXCoord().getEncoded()));
        System.out.println("pubKey y: " + Hex.encode(sm2KeyPair.getPublicKey().getYCoord().getEncoded()));
        Assert.assertNotNull(sm2KeyPair);

    }

    /**
     * 生成sm2公私钥进行签名并校验
     */
    @Test
    public void testSignAndVerifyFromGenKey() {

        SM2Impl sm2 = new SM2Impl();
        SM2KeyPair sm2KeyPair = sm2.generateValidKeyPair();
        byte[] msg = "test3424".getBytes();

        System.out.println("msg:      " + Hex.encode(msg));
        System.out.println("priKey:   " + Hex.encode(sm2KeyPair.getPrivateKey().toByteArray()));
        System.out.println("pubKey:   " + Hex.encode(sm2KeyPair.getPublicKey().getEncoded()));
        System.out.println("pubKey x: " + Hex.encode(sm2KeyPair.getPublicKey().getXCoord().getEncoded()));
        System.out.println("pubKey y: " + Hex.encode(sm2KeyPair.getPublicKey().getYCoord().getEncoded()));

        // 并发测试
//        ExecutorService executor = Executors.newCachedThreadPool();
//        for (int i = 0; i < 100; i++) {
//            executor.submit(() -> {
//                System.out.println(Thread.currentThread().getId());
//                byte[] sign = sm2.sign(sm2KeyPair, msg, SM2.DEFAULT_IDA);
//                Assert.assertTrue(sm2.verify(msg, sm2KeyPair.getPublicKey(), sign, SM2.DEFAULT_IDA));
//
//            });
//        }
//        executor.shutdown();

        // 串行测试
        byte[] sign = sm2.sign(sm2KeyPair, msg, SM2.DEFAULT_IDA);
        SM2Impl.Signature signature = new SM2Impl.Signature(sign);

        System.out.println("sign:     " + Hex.encode(sign));
        if (signature.getS().startsWith("00") || signature.getR().startsWith("00")) {
            System.out.println("sign r:   " + signature.getR());
            System.out.println("sign s:   " + signature.getS());
            System.out.println();
        }

        Assert.assertTrue(sm2.verify(msg, sm2KeyPair.getPublicKey(), sign, SM2.DEFAULT_IDA));

    }


    /**
     * sk文件获取公私钥数据进行签名校验
     */
    @Test
    public void testSignAndVerifyFromSK() {
        // hex msg
        byte[] msg = "111".getBytes();
        String skKeyPath = "D:\\key\\Admin@org1.zjl.com\\7273d74004dbc6a72023a9dbb543ec30ad7ce3a60107a3bd81f167e953f355a2_sk";
        String ida = SM2.DEFAULT_IDA;
        Reader pemReader = null;
        File fileSk = new File(skKeyPath);
        if (!fileSk.exists()){
            System.out.println("文件不存在: "+skKeyPath);
            System.out.println("跳过测试");
            return;
        }
        try {
            pemReader = new BufferedReader(new FileReader(fileSk));
        } catch (FileNotFoundException e) {
            System.out.println(skKeyPath + "文件不存在");
            System.exit(0);
        }

        PrivateKeyInfo pemPair = null;
        try (PEMParser pemParser = new PEMParser(pemReader)) {
            pemPair = (PrivateKeyInfo) pemParser.readObject();
        } catch (IOException e) {
            System.out.println("证书解析错误!");
            System.exit(0);
        }

        PrivateKey privateKey = null;
        try {
            Security.addProvider(new BouncyCastleProvider());
            privateKey = new JcaPEMKeyConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME).getPrivateKey(pemPair);
        } catch (PEMException e) {

            System.exit(0);
        }

        ECPrivateKey ecPrivateKey = (ECPrivateKey) privateKey;
        SM2Impl sm2 = new SM2Impl();
        SM2KeyPair sm2KeyPair = sm2.genKeypairByPrivateKey(ecPrivateKey.getS());

        byte[] sign = sm2.sign(sm2KeyPair, msg, ida);
        Assert.assertNotNull(sign);

        SM2Impl.Signature signature = new SM2Impl.Signature(sign);

        System.out.println("msg:      " + Hex.encode(msg));
        System.out.println("priKey:   " + Hex.encode(sm2KeyPair.getPrivateKey().toByteArray()));
        System.out.println("pubKey:   " + Hex.encode(sm2KeyPair.getPublicKey().getEncoded()));
        System.out.println("pubKey x: " + Hex.encode(sm2KeyPair.getPublicKey().getXCoord().getEncoded()));
        System.out.println("pubKey y: " + Hex.encode(sm2KeyPair.getPublicKey().getYCoord().getEncoded()));
        System.out.println("sign:     " + Hex.encode(sign));
        System.out.println("sign r:   " + signature.getR());
        System.out.println("sign s:   " + signature.getS());

        boolean verify = sm2.verify(msg, sm2KeyPair.getPublicKey(), sign, ida);
        Assert.assertTrue(verify);

    }

    /**
     * 通过公私钥生成sm2keyPart进行签名
     */
    @Test
    public void testSignByKeyPart() {
        SM2Impl sm2 = new SM2Impl();

        byte[] msg = Hex.decode("7465737433343234");
        // hex pubKey数据
        String priKey = "3756346e43268df626d8c476bced67a8643319775073057cd6327f8a0ccd68c6";
        String pubkey = "04248e63e1827f282924bdc6346dd7bab8506a4e0ceceba31ba4173a7692e430bfb9bbb6dd3bf5a23cb816bddfad37731b33dc6cfaffc5410785fc6a3ff66ed1fc";
        // hex sign 数据
        String ida = SM2.DEFAULT_IDA;

        SM2KeyPair sm2KeyPair = genByHexString(pubkey, priKey);
        byte[] sign = sm2.sign(sm2KeyPair, msg, ida);
        Assert.assertNotNull(sign);
        System.out.println("sign:\t" + Hex.encode(sign));

        assertTrue(sm2.verify(msg, sm2KeyPair.getPublicKey(), sign, ida));
    }

    @Test
    public void cc() throws UnsupportedEncodingException {

        byte[] b1 = Hex.decode("87ef");

        byte[] bx = new String(b1, "GBK").getBytes("UTF-8");

        System.out.println(Hex.encode(bx));

    }

    /**
     * hex pubKey校验函数
     */
    @Test
    public void testVrtifyFromPubKey() throws UnsupportedEncodingException {
        SM2Impl sm2 = new SM2Impl();

        // hex msg 消息
        byte[] msg = Hex.decode("32d32de2c874ca");

        // hex pubKey数据
        String pubKey = "04" + "7c15157b63fe4f83bd6aebb692a1830db254dbc09bd93826ae5893eb3fa33d49" + "87d59b5c5188c611fca12a26b59c9c7660cdbe84c79ce599ed178a525d017435";

        // hex sign 数据
        byte[] sign = Base64.getDecoder().decode("MEYCIQD929cu11gsoi1smxPNdvMHGWclJPS0Jfgh8Z+fSfyAkAIhAIgNTLpLtmrnuzDwmJEfH9QtZgxpmVrkp4639ugZOjqJ");

        System.out.println(Hex.encode(sign));

        String ida = SM2.DEFAULT_IDA;

        ECPoint ecPoint = sm2.decodePublicKey(pubKey);

        Assert.assertTrue(sm2.verify(msg, ecPoint, sign, ida));

    }

    @Test
    public void testVrtifyFromPem() {

        Security.addProvider(new BouncyCastleProvider());
        String ida = SM2.DEFAULT_IDA;
        CertificateFactory certFactory;
        Certificate certificate = null;
        BufferedInputStream pem = null;


        String pemStr = "-----BEGIN CERTIFICATE-----\n" +
                "MIIE4DCCBIegAwIBAgIKGhAAAAAAAMOH9jAKBggqgRzPVQGDdTBEMQswCQYDVQQG\n" +
                "EwJDTjENMAsGA1UECgwEQkpDQTENMAsGA1UECwwEQkpDQTEXMBUGA1UEAwwOQmVp\n" +
                "amluZyBTTTIgQ0EwHhcNMTgwODMxMTYwMDAwWhcNMjgwOTAxMTU1OTU5WjCB3DEb\n" +
                "MBkGA1UEKQwSOTExMTAxMDdNQTAwR1VBRzA0MTAwLgYDVQQDDCfljJfkuqznn6Xp\n" +
                "h5Hpk77nvZHnu5zmioDmnK/mnInpmZDlhazlj7gxGDAWBgNVBAsMD0RTMTIwRzMx\n" +
                "ODA4MTE3ODENMAsGA1UECgwEQkpDQTEwMC4GA1UECgwn5YyX5Lqs55+l6YeR6ZO+\n" +
                "572R57uc5oqA5pyv5pyJ6ZmQ5YWs5Y+4MREwDwYDVQQHDAggQmVpSmluZzEQMA4G\n" +
                "A1UECAwHQmVpSmluZzELMAkGA1UEBgwCQ04wWTATBgcqhkjOPQIBBggqgRzPVQGC\n" +
                "LQNCAAQUSpEMQJg6y5VmnUmRCOdS7nbl+p4C/RdSlmyFABZQtSlUdh373CCF/mO8\n" +
                "idiUKC1xMs6FfLEF4EKx2pKL1Kr8o4ICxjCCAsIwHwYDVR0jBBgwFoAUH+bP1I/F\n" +
                "IiqXSimKFecWyZI0xLYwHQYDVR0OBBYEFGGkmszF+sABHmfUeTORkyEiIW59MAsG\n" +
                "A1UdDwQEAwIGwDCBnQYDVR0fBIGVMIGSMGCgXqBcpFowWDELMAkGA1UEBhMCQ04x\n" +
                "DTALBgNVBAoMBEJKQ0ExDTALBgNVBAsMBEJKQ0ExFzAVBgNVBAMMDkJlaWppbmcg\n" +
                "U00yIENBMRIwEAYDVQQDEwljYTIxY3JsNjUwLqAsoCqGKGh0dHA6Ly9jcmwuYmpj\n" +
                "YS5vcmcuY24vY3JsL2NhMjFjcmw2NS5jcmwwJAYKKoEchu8yAgEBAQQWDBRKSjkx\n" +
                "MTEwMTA3TUEwMEdVQUcwNDBgBggrBgEFBQcBAQRUMFIwIwYIKwYBBQUHMAGGF09D\n" +
                "U1A6Ly9vY3NwLmJqY2Eub3JnLmNuMCsGCCsGAQUFBzAChh9odHRwOi8vY3JsLmJq\n" +
                "Y2Eub3JnLmNuL2NhaXNzdWVyMEAGA1UdIAQ5MDcwNQYJKoEchu8yAgIBMCgwJgYI\n" +
                "KwYBBQUHAgEWGmh0dHA6Ly93d3cuYmpjYS5vcmcuY24vY3BzMBEGCWCGSAGG+EIB\n" +
                "AQQEAwIA/zAiBgoqgRyG7zICAQEIBBQMEjkxMTEwMTA3TUEwMEdVQUcwNDAkBgoq\n" +
                "gRyG7zICAQICBBYMFEpKOTExMTAxMDdNQTAwR1VBRzA0MB8GCiqBHIbvMgIBAQ4E\n" +
                "EQwPMTAyMDAwMDExNzQ0NDYyMCQGCiqBHIbvMgIBAQQEFgwUSko5MTExMDEwN01B\n" +
                "MDBHVUFHMDQwLgYKKoEchu8yAgEBFwQgDB4yNUAyMTUwMDlKSjA5MTExMDEwN01B\n" +
                "MDBHVUFHMDQwIAYIKoEc0BQEAQQEFAwSOTExMTAxMDdNQTAwR1VBRzA0MBMGCiqB\n" +
                "HIbvMgIBAR4EBQwDNjU0MAoGCCqBHM9VAYN1A0cAMEQCIDkwFmeRvZcy0rmAcMwT\n" +
                "h2UJhoCtWUjFVM/rLmKmq+lBAiAeQY9cr+Exvir8V8XT8WO54OvfwejIgeY9Q0Fz\n" +
                "qX0Eag==\n" +
                "-----END CERTIFICATE-----";

        byte[] msg = "ads".getBytes(StandardCharsets.UTF_8);

        byte[] sign = Base64.getDecoder().decode("MEQCIHb2wwO69vDwTlbSzbflnIUxOUGUQM6dddPI4NBVer42AiBiPv/Bep0HM3WI5SVXUTh2WeFmemYHQ/FWxCUrIbedsA==");
        sign = Hex.decode("3046022100bf5cdf62ab468b0100e0fa0a6022c8487b0b15310de319dc92efa23f9e9d783e022100f6a08d83fda53f442de9301f8703d40007a5b00340aa6ae21269967c5a5ed10d");

        System.out.println("hex sign :" + Hex.encode(sign));


        try {
            pem = new BufferedInputStream(new ByteArrayInputStream(pemStr.getBytes()));
            certFactory = CertificateFactory.getInstance("X.509", "BC");
            certificate = certFactory.generateCertificate(pem);
        } catch (Exception e) {
            System.out.println("获取证书实例出现异常");
            System.exit(0);
        } finally {
            try {
                if (null != pem) {
                    pem.close();
                }
            } catch (IOException e) {
                System.out.println("关闭pem流出现异常");
            }
        }

        if (null == certificate) {
            System.out.println("证书获取失败");
            System.exit(0);
        }

        String sc = Hex.encode(certificate.getPublicKey().getEncoded());
        String pubkeyStr = "04" + sc.substring(sc.length() - 128);
        System.out.println("hex pukKey: " + pubkeyStr);

        SM2Impl sm2 = new SM2Impl();
        ECPoint ecPoint = sm2.decodePublicKey(pubkeyStr);
        Assert.assertTrue(sm2.verify(msg, ecPoint, sign, ida));

    }

}
