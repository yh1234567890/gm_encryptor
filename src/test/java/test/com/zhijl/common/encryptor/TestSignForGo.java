package test.com.zhijl.common.encryptor;

import com.zhijl.common.encryptor.asymmetric.SM2;
import com.zhijl.common.encryptor.asymmetric.SM2Impl;
import com.zhijl.common.encryptor.asymmetric.SM2KeyPair;
import com.zhijl.common.encryptor.bytetool.Hex;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class TestSignForGo {

    @Test
    public void generateKey() throws IOException {
        SM2 sm2 = new SM2Impl();

        SM2KeyPair sm2KeyPair = sm2.generateValidKeyPair();
        String Xa = Hex.encode(sm2KeyPair.getPublicKey().getXCoord().toBigInteger().toByteArray());
        String Ya = Hex.encode(sm2KeyPair.getPublicKey().getYCoord().toBigInteger().toByteArray());
        String spri = sm2.encodePrivateKey(sm2KeyPair.getPrivateKey());
        System.out.println("生成的Xa :" + Xa);
        System.out.println("生成的Ya :" + Ya);
        System.out.println("生成的Ya1:" +  sm2KeyPair.getPublicKey().getYCoord().toBigInteger().toString(16));
        System.out.println("生成的私钥:" + spri);

        // 杂凑函数值的字节数组
        byte[] hash = Hex.decode("F0B43E94 BA45ACCA ACE692ED 534382EB 17E6AB5A 19CE7B31 F4486FDF C0D28640".replaceAll(" ",""));

        byte[] signdata = sm2.sign(sm2KeyPair,hash, SM2.DEFAULT_IDA);

        System.out.println("生成的签名数据:" + Hex.encode(signdata));

        //----------------------------------------------------------------------------------
        //初始化密钥
        String pub = "04" +
                //        坐标xA：
                "3edaa6379e52a61fef055124823800a4270d49360032f72c4d2780b88c370784" +
                //        坐标yA：
                "4104641d99c5d519ebff4071dff20f3029a9769d79d21c9e49fde70972de026b";
        String pri = "00ca265379d8153ce7a2e6ab5867babe6a5e328cf696c13d08605669356cc78317";
        sm2KeyPair = SM2KeyPair.genByHexString(pub, pri);

        signdata = Hex.decode("30440220731433096d188e153ae53c5ef3eac0e51cded8b3db3a31f8582e3d6c964b69d802206977ed59f2e4a85f3bd419677918fc1b965cb1967d33679f6e491a973d6cb37a");

        //----------------------------------------------------------------------------------
        /*//初始化密钥
        String pub = "04" +
                //        坐标xA：
                "f8fd8d6bfb46bb67eb7efdb20533fed9512a650a0ad6832ed9baecd50588cf88" +
                //        坐标yA：
                "05bcba7b4a7568c19e89fafebd4c09577b987750c2ad9232eee6ed4f8045a93a";
        String pri = "54ccd85abbb06f07a1f3c874e29efbe5148d0824a55e8882266b889c4f8a8a98";
        SM2KeyPair sm2KeyPair = SM2KeyPair.genByHexString(pub, pri);

        byte[] signdata = Hex.decode("3046022100f5a03b0648d2c4630eeac513e1bb81a15944da3827d5b74143ac7eaceee720b3022100dc99619fc79141516c657e4f94ede0f496521a542480776b68c8cbc3d77eca6c");*/

        //----------------------------------------------------------------------------------
        /*//初始化密钥
        String pub = "04" +
                //        坐标xA：
                "5dd7fef3754a55a98036a385b4da358b5ea7c6c6e1f6f6d3510e96e3416cc26e" +
                //        坐标yA：
                "8b4214682d2a4a6beb557339e111f78477b1d804508addc02ebee66abfe34898";
        String pri = "cd367ebe116ef7faac694a29e5584a306eb6c9aa14a68721f7fbbd9f4160b277";
//        SM2KeyPair sm2KeyPair = SM2KeyPair.genByHexString(pub, pri);

        byte[] signdata = Hex.decode("3046022100f5a03b0648d2c4630eeac513e1bb81a15944da3827d5b74143ac7eaceee720b302210084a948b8e40e6ac849713c6378f91c5ca4721e842d00b6dd34eb6e3c9751e4e8");*/
        //校验本地的verify方法
        assertTrue(
                sm2.verify(
                        hash,
                        sm2KeyPair.getPublicKey(),
                        signdata,
                        SM2.DEFAULT_IDA
                )
        );
    }
}
