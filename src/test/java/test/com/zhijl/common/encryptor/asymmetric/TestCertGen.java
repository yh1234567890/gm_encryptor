package test.com.zhijl.common.encryptor.asymmetric;

import com.zhijl.common.encryptor.asymmetric.SM2Impl;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidParameterSpecException;
import java.util.Base64;
import java.util.Date;

/**
 * 〈〉
 *
 * @author Administrator
 * @date 2018/8/20
 * @since 1.0.0
 */
public class TestCertGen {

    @Before
    public void init(){
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    public void testGenCert() throws NoSuchAlgorithmException, OperatorCreationException, CertificateException, IOException, InvalidAlgorithmParameterException, NoSuchProviderException {

        ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec("sm2p256v1");
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDSA", "BC");
        keyPairGenerator.initialize(parameterSpec);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        X500Name subjectDN = new X500Name("CN=RootCA,OU=hackwp,O=wp,L=BJ,C=CN");
        BigInteger serialNumber = BigInteger.valueOf(System.currentTimeMillis());
        Date startDate = new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
        Date endDate = new Date(System.currentTimeMillis() + 365 * 24 * 60 * 60 * 1000);
        SubjectPublicKeyInfo subPubKeyInfo = SubjectPublicKeyInfo.getInstance(keyPair.getPublic().getEncoded());

        X509v3CertificateBuilder builder = new X509v3CertificateBuilder(subjectDN, serialNumber, startDate,
                endDate, subjectDN, subPubKeyInfo);

        ContentSigner signer = new JcaContentSignerBuilder("SM3WITHSM2").setProvider("BC").build(keyPair.getPrivate());

        X509CertificateHolder holder = builder.build(signer);

        X509Certificate certificate = new JcaX509CertificateConverter().setProvider("BC").getCertificate(holder);

        String preCert = "-----BEGIN CERTIFICATE-----\n";
        String presk = "-----BEGIN PRIVATE KEY-----\n";

        String endCert = "\n-----END CERTIFICATE-----";
        String endsk = "\n-----END PRIVATE KEY-----";


        System.out.println(StringUtils.join(
                "pem File is: \n",
                preCert,
                Base64.getEncoder().encodeToString(certificate.getEncoded()),
                endCert
                )
        );

        System.out.println(
                StringUtils.join(
                        "sk File is: \n",
                        presk,
                        Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()),
                        endsk
                )
        );

    }

    @Test
    public void test_method_genSelfedCert(){
        SM2Impl sm2 = new SM2Impl();
        System.out.println(sm2.genSelfedCert());
    }


    @Test
    public void testGenGMKeyPairByBC() throws NoSuchAlgorithmException, InvalidParameterSpecException, NoSuchProviderException, InvalidAlgorithmParameterException {

        Security.addProvider(new BouncyCastleProvider());

        ECNamedCurveParameterSpec parameterSpec = ECNamedCurveTable.getParameterSpec("sm2p256v1");

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("ECDSA", "BC");

        keyPairGenerator.initialize(parameterSpec);

        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        System.out.println(keyPair);
        
    }


}
