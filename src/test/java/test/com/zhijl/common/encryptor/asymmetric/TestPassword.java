package test.com.zhijl.common.encryptor.asymmetric;

import com.zhijl.common.encryptor.asymmetric.SM2;
import com.zhijl.common.encryptor.asymmetric.SM2Impl;
import com.zhijl.common.encryptor.asymmetric.SM2SelfedCert;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * 〈〉
 *
 * @author Administrator
 * @date 2018/8/31
 * @since 1.0.0
 */
public class TestPassword {

    @Test
    public void testSm2SelfedCert() throws IOException {
        SM2Impl sm2 = new SM2Impl();
        byte[] key = {
                0x01, 0x23, 0x45, 0x67, (byte) 0x89, (byte) 0xab, (byte) 0xcd, (byte) 0xef,
                (byte) 0xfe, (byte) 0xdc, (byte) 0xba, (byte) 0x98, 0x76, 0x54, 0x32, 0x10
        };

        String password = "ppppppp0";
        System.out.println("password length is " + password.length());
        SM2SelfedCert sm2SelfedCert = sm2.genSelfedCertObj();
        String temp = sm2SelfedCert.getSkCert();
        sm2SelfedCert.encryptSKBySM4(password);
        sm2SelfedCert.decryptSKBySM4(password);

        Assert.assertEquals(temp, sm2SelfedCert.getSkCert());

    }

    @Test
    public void testSm2Password() throws IOException {
        String msg = "这俩货sand2321@#@";

        SM2 sm2 = new SM2Impl();

        String password = "@#@%@^#%&**";

        System.out.println("password length is " + password.length());

        // 生成密码加密的公私钥证书对
        SM2SelfedCert sm2SelfedCert = sm2.genSelfedCertWithPassword(password);

        // 密码签名
        String enSign = sm2.signWithPassword(sm2SelfedCert.getSkCert(), password, msg);
        System.out.println("ensign: " + enSign);

        // 密码校验
        Assert.assertTrue(sm2.verifyByPemCert(msg, sm2SelfedCert.getPemCert(), enSign, SM2.DEFAULT_IDA));

    }

}
