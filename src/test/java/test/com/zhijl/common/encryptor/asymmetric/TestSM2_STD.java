package test.com.zhijl.common.encryptor.asymmetric;

import com.zhijl.common.encryptor.asymmetric.SM2;
import com.zhijl.common.encryptor.asymmetric.SM2Impl;
import com.zhijl.common.encryptor.asymmetric.SM2KeyPair;
import com.zhijl.common.encryptor.bytetool.Hex;
import org.bouncycastle.math.ec.ECPoint;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestSM2_STD {

    /**
     * 测试签名
     * 《第2部分：数字签名算法》示例
     * 该示例采用的是上述的参数，非最终推荐参数
     *
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testSignPart2() throws UnsupportedEncodingException {

        //设置曲线参数 how to init? i don't know.
        SM2Impl.changeParamToTest();
        //指定例子中的随机数
        SM2Impl.setRandom(new BigInteger(
                "6CB28D99 385C175C 94F94E93 4817663F C176D925 DD72B727 260DBAAE 1FB2F96F".replaceAll(" ", ""), 16)
        );
        //创建对象
        SM2 sm2 = new SM2Impl();

        //初始化密钥
        String pub = "04" +
                //        坐标xA：
                "0AE4C779 8AA0F119 471BEE11 825BE462 02BB79E2 A5844495 E97C04FF 4DF2548A".replaceAll(" ", "") +
                //        坐标yA：
                "7C0240F8 8F1CD4E1 6352A73C 17B7F16F 07353E53 A176D684 A9FE0C6B B798E857".replaceAll(" ", "");
        String pri = "128B2FA8 BD433C6C 068C8D80 3DFF7979 2A519A55 171B1B65 0C23661D 15897263".replaceAll(" ", "");
        SM2KeyPair sm2KeyPair1 = SM2KeyPair.genByHexString(pub, pri);

        String IDA = "ALICE123@YAHOO.COM";
        String M = "message digest";//待签名信息
        byte[] signdata = sm2.sign( sm2KeyPair1,M.getBytes("UTF-8"), IDA);

        String expectR = "40F1EC59 F793D9F4 9E09DCEF 49130D41 94F79FB1 EED2CAA5 5BACDB49 C4E755D1".replaceAll(" ", "").toLowerCase();
        String expectS = "6FC6DAC3 2C5D5CF1 0C77DFB2 0F7C2EB6 67A45787 2FB09EC5 6327A67E C7DEEBE7".replaceAll(" ", "").toLowerCase();

        SM2Impl.Signature signature = new SM2Impl.Signature(signdata);
        //校验是否一致
        assertEquals(expectR, signature.getR());
        assertEquals(expectS, signature.getS());

        //校验本地的verify方法
        assertTrue(
                sm2.verify(
                        M.getBytes("UTF-8"),
                        sm2KeyPair1.getPublicKey(),
                        signdata,
                        IDA
                )
        );
    }

    /**
     * 测试公钥加密
     * 《第4部分：公钥加密算法》示例
     * 该示例采用的是上述的参数，非最终推荐参数
     *
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testPublicEncryptPart4() throws UnsupportedEncodingException {

        //设置曲线参数
        SM2Impl.changeParamToTest();
        //指定例子中的随机数
        SM2Impl.setRandom(new BigInteger(
                "4C62EEFD 6ECFC2B9 5B92FD6C 3D957514 8AFA1742 5546D490 18E5388D 49DD7B4F".replaceAll(" ", ""), 16)
        );
        //创建对象
        SM2Impl sm2 = new SM2Impl();
        sm2.setDebug(true);

        //初始化密钥
        String pub = "04" +
                //        坐标xA：
                "435B39CC A8F3B508 C1488AFC 67BE491A 0F7BA07E 581A0E48 49A5CF70 628A7E0A".replaceAll(" ", "") +
                //        坐标yA：
                "75DDBA78 F15FEECB 4C7895E2 C1CDF5FE 01DEBB2C DBADF453 99CCF77B BA076A42".replaceAll(" ", "");
        String pri = "1649AB77 A00637BD 5E2EFE28 3FBF3535 34AA7F7C B89463F2 08DDBC29 20BB0DA0".replaceAll(" ", "");
        SM2KeyPair sm2KeyPair1 = SM2KeyPair.genByHexString(pub, pri);

        String M = "encryption standard";

        byte[] data = sm2.encrypt(M.getBytes(), sm2KeyPair1.getPublicKey());

        String expect = ("04245C26 FB68B1DD DDB12C4B 6BF9F2B6 D5FE60A3 83B0D18D 1C4144AB F17F6252" +
                "E776CB92 64C2A7E8 8E52B199 03FDC473 78F605E3 6811F5C0 7423A24B 84400F01" +
                "B8650053 A89B41C4 18B0C3AA D00D886C 00286467 9C3D7360 C30156FA B7C80A02" +
                "76712DA9 D8094A63 4B766D3A 285E0748 0653426D").replaceAll(" ", "").toLowerCase();

        assertEquals(expect, Hex.encode(data));

        //解密
        System.out.println(sm2.decrypt(data, sm2KeyPair1.getPrivateKey()));

    }

    /**
     * 测试签名
     * 《第5部分：参数定义》示例
     * 该示例采用推荐参数
     */
    @Test
    public void testSignWithRecommendParam() throws UnsupportedEncodingException {
        SM2Impl.changeParamToRecommand();
        //指定例子中的随机数
        SM2Impl.setRandom(new BigInteger(
                "59276E27 D506861A 16680F3A D9C02DCC EF3CC1FA 3CDBE4CE 6D54B80D EAC1BC21".replaceAll(" ", ""), 16)
        );
        //创建对象
        SM2 sm2 = new SM2Impl();

        //初始化密钥
        String pub = "04" +
                //        坐标xA：
                "09F9DF31 1E5421A1 50DD7D16 1E4BC5C6 72179FAD 1833FC07 6BB08FF3 56F35020".replaceAll(" ", "") +
                //        坐标yA：
                "CCEA490C E26775A5 2DC6EA71 8CC1AA60 0AED05FB F35E084A 6632F607 2DA9AD13".replaceAll(" ", "");
        String pri = "3945208F 7B2144B1 3F36E38A C6D39F95 88939369 2860B51A 42FB81EF 4DF7C5B8".replaceAll(" ", "");
        SM2KeyPair sm2KeyPair1 = SM2KeyPair.genByHexString(pub, pri);

        String IDA = "1234567812345678";
        String M = "message digest";//待签名信息
        byte[] signdata = sm2.sign(sm2KeyPair1,M.getBytes("UTF-8"), IDA);

        String expectR = "F5A03B06 48D2C463 0EEAC513 E1BB81A1 5944DA38 27D5B741 43AC7EAC EEE720B3".replaceAll(" ", "").toLowerCase();
        String expectS = "B1B6AA29 DF212FD8 763182BC 0D421CA1 BB9038FD 1F7F42D4 840B69C4 85BBC1AA".replaceAll(" ", "").toLowerCase();

        SM2Impl.Signature signature = new SM2Impl.Signature(signdata);

        // fixme liu 2018/8/30 12:17 校验可以通过但是单独拿rs多了00
        //校验是否一致
        assertEquals(expectR, signature.getR());
        assertEquals(expectS, signature.getS());

        //校验本地的verify方法
        assertTrue(
                sm2.verify(
                        M.getBytes("UTF-8"),
                        sm2KeyPair1.getPublicKey(),
                        signdata,
                        IDA
                )
        );
    }


    /**
     * 本地方法测试
     * 测试生成密钥对后的编码、解码
     */
    @Test
    public void testGenAndEncode() {

        SM2 sm2 = new SM2Impl();

        for (int i = 0; i < 10; i++) {
            SM2KeyPair sm2KeyPair = sm2.generateValidKeyPair();

            String spub = sm2.encodePublicKey(sm2KeyPair.getPublicKey());
            String spri = sm2.encodePrivateKey(sm2KeyPair.getPrivateKey());

            System.out.println("生成的公钥 ，" + spub.length() + "," + spub);
            System.out.println("生成的私钥 ，" + spri.length() + "," + spri);

            assertEquals(130, spub.length());
            SM2KeyPair sm2KeyPair_gen = SM2KeyPair.genByHexString(spub, spri);

            System.out.println("regen的公钥，" + sm2.encodePublicKey(sm2KeyPair_gen.getPublicKey()));
            System.out.println("regen的私钥，" + sm2.encodePrivateKey(sm2KeyPair_gen.getPrivateKey()));

            // 解析，然后比对
            assertEquals(sm2.encodePrivateKey(sm2KeyPair_gen.getPrivateKey()), spri);
            assertEquals(sm2.encodePublicKey(sm2KeyPair_gen.getPublicKey()), spub);
        }
    }


    /**
     * 测试公钥加密
     */
    @Test
    public void testEncryptPart4() throws UnsupportedEncodingException {
        SM2Impl.changeParamToRecommand();
        SM2 sm2 = new SM2Impl();

        SM2KeyPair keyPair = sm2.generateValidKeyPair();
        ECPoint publicKey = keyPair.getPublicKey();
        BigInteger privateKey = keyPair.getPrivateKey();

        //公钥
        String encode_publickey = sm2.encodePublicKey(publicKey);
        System.out.println("公钥编码:" + encode_publickey);

        //测试公钥的编解码
        assertEquals(encode_publickey,
                sm2.encodePublicKey(sm2.decodePublicKey(encode_publickey)));

        //私钥
        String encode_privatekey = sm2.encodePrivateKey(privateKey);
        System.out.println("私钥编码:" + encode_privatekey);

        //测试私钥的编解码
        assertEquals(encode_privatekey,
                sm2.encodePrivateKey(sm2.decodePrivateKey(encode_privatekey)));

        byte[] in = new byte[]{
                0x65, 0x6E, 0x63, 0x72, 0x79, 0x70, 0x74, 0x69,
                0x6F, 0x6E, 0x20, 0x73, 0x74, 0x61, 0x6E, 0x64,
                0x61, 0x72, 0x64};
        byte[] data = sm2.encrypt(in, publicKey);
        System.out.println("密文base64:" + Hex.encode(data));

        //解密
        System.out.println("解密后明文:" + sm2.decrypt(data, privateKey));
        assertEquals(sm2.decrypt(data, privateKey), new String(in, "utf-8"));
    }

    /**
     * 测试sm2密钥交换
     */
    @Test
    public void testExchange() {
        System.out.println("-------------开始测试sm2密钥协商");

        SM2 sm02 = new SM2Impl();

        //手动指定私钥，公钥。用于测试效果
//        BigInteger px = new BigInteger(
//                "435B39CC A8F3B508 C1488AFC 67BE491A 0F7BA07E 581A0E48 49A5CF70 628A7E0A".replace(" ", ""), 16);
//        BigInteger py = new BigInteger(
//                "75DDBA78 F15FEECB 4C7895E2 C1CDF5FE 01DEBB2C DBADF453 99CCF77B BA076A42".replace(" ", ""), 16);
//        publicKey = sm02.curve.createPoint(px, py);
//        privateKey = new BigInteger(
//                "1649AB77 A00637BD 5E2EFE28 3FBF3535 34AA7F7C B89463F2 08DDBC29 20BB0DA0".replace(" ", ""), 16);

        String aID = "AAAAAAAAAAAAA";
        SM2KeyPair aKeyPair = sm02.generateValidKeyPair();
        SM2Impl.KeyExchange aKeyExchange = new SM2Impl.KeyExchange(aID, aKeyPair);

        String bID = "BBBBBBBBBBBBB";
        SM2KeyPair bKeyPair = sm02.generateValidKeyPair();
        SM2Impl.KeyExchange bKeyExchange = new SM2Impl.KeyExchange(bID, bKeyPair);

        SM2Impl.TransportEntity entity1 = aKeyExchange.keyExchange_1();
        SM2Impl.TransportEntity entity2 = bKeyExchange.keyExchange_2(entity1);
        SM2Impl.TransportEntity entity3 = aKeyExchange.keyExchange_3(entity2);
        bKeyExchange.keyExchange_4(entity3);
    }
}
