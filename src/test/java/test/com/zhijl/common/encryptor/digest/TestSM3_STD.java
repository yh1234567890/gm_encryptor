package test.com.zhijl.common.encryptor.digest;


import com.zhijl.common.encryptor.bytetool.Hex;
import com.zhijl.common.encryptor.digest.SM3;
import com.zhijl.common.encryptor.digest.SM3Impl;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TestSM3_STD {


    SM3 sm3Impl = new SM3Impl();

    @Test
    public void testSm3() throws IOException {
        {
            //国密标准  示例1
            //输入
            String input = "abc";
            //期望值
            String expect = "66c7f0f4 62eeedd9 d1f2d46b dc10e4e2 4167c487 5cf2f7a2 297da02b 8f4ba8e0".replaceAll(" ","");
            //校验
            sm3Impl.update(input.getBytes());
            assertEquals(
                    expect,
                    Hex.encode(sm3Impl.digest())
            );

            //国密标准 示例2
            //输入
            byte[] input_bytes = new byte[]{
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64,
                    0x61, 0x62, 0x63, 0x64, 0x61, 0x62, 0x63, 0x64};
            sm3Impl.update(input_bytes);
            //期望值
            expect = "debe9ff9 2275b8a1 38604889 c18e5a4d 6fdb70e5 387e5765 293dcba3 9c0c5732".replaceAll(" ","");
            //校验
            assertEquals(
                    expect,
                    Hex.encode(sm3Impl.digest())
            );

            for (int i = 0; i < 1000000; i++){
                sm3Impl.update((byte) 'a');
            }
            assertEquals(
                    "C8AAF89429554029E231941A2ACC0AD61FF2A5ACD8FADD25847A3A732B3B02C3",
                    Hex.encode(sm3Impl.digest()).toUpperCase()
            );

            /**
             * 性能测试
             * 文件大小  时间
             * 1.5g       67.3s
             * 512M       26.0s
             * 215M        9.6s
             * 19M          810ms
             * 5.4M         230ms
             */
//            long start = System.currentTimeMillis();
//            for (int i=0;i<20;i++){
//                String s = Hex.encode(sm3Impl.getFileHash("D:\\情报学基础教程.pdf"));
//            }
//            System.out.println("time=" +( System.currentTimeMillis()-start));

        }
    }
}
