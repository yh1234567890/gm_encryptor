package test.com.zhijl.common.encryptor.symmetric;

import com.zhijl.common.encryptor.bytetool.Hex;
import com.zhijl.common.encryptor.symmetric.SM4;
import com.zhijl.common.encryptor.symmetric.SM4Impl;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 参照标准:《GM/T 0002-2012 SM4分组密码算法》
 * 该示例采用的是上述参数
 */
public class TestSM4_STD {

    // 加密对象
    SM4 sm4Impl = new SM4Impl();
    // 加解密标识，加密1，解密0
    int ENCRYPT = 1, DECRYPT = 0;

    // 测试明文
    byte[] in = {
            0x01, 0x23, 0x45, 0x67, (byte) 0x89, (byte) 0xab, (byte) 0xcd, (byte) 0xef,
            (byte) 0xfe, (byte) 0xdc, (byte) 0xba, (byte) 0x98, 0x76, 0x54, 0x32, 0x10
    };

    // 测试密钥
    byte[] key = {
            0x01, 0x23, 0x45, 0x67, (byte) 0x89, (byte) 0xab, (byte) 0xcd, (byte) 0xef,
            (byte) 0xfe, (byte) 0xdc, (byte) 0xba, (byte) 0x98, 0x76, 0x54, 0x32, 0x10
    };

    // 接收加密后的密文
    byte[] out = new byte[16];
    // 接收解密后的明文
    byte[] deOut = new byte[16];


    /**
     * 示例1:
     *    对一组明文进行加密一次
     */
    @Test
    public void testOneGroupEncrypt() {
        // 期望值
        byte[] expectEncrpt = {
                0x68, (byte) 0x1e, (byte) 0xdf, 0x34, (byte) 0xd2, 0x06, (byte) 0x96, (byte) 0x5e,
                (byte) 0x86, (byte) 0xb3, (byte) 0xe9, (byte) 0x4f, 0x53, (byte) 0x6e, 0x42, 0x46
        };

        // 加密
        sm4Impl.sms4(in, in.length, key, out, ENCRYPT);

        print("明文:");
        printBytesToInt(in);

        print("密钥:");
        printBytesToInt(key);

        print("密文:");
        printBytesToInt(out);

        // 校验加密
        assertEquals(Hex.encode(expectEncrpt), Hex.encode(out));

        // 解密
        sm4Impl.sms4(out, out.length, key, deOut, DECRYPT);

        print("解密:");
        printBytesToInt(deOut);

        // 校验解密
        assertEquals(Hex.encode(deOut), Hex.encode(in));

    }

    /**
     * 示例2:
     *    对一组明文反复加密 1 000 000次
     */
    @Test
    public void testOneGroupEncryptMillionTimes() {
        // 期望值
        byte[] expectEncrpt = {
                0x59, 0x52, (byte) 0x98, (byte) 0xc7, (byte) 0xc6, (byte) 0xfd, 0x27, (byte) 0x1f,
                0x04, 0x02, (byte) 0xf8, 0x04, (byte) 0xc3, (byte) 0x3d, (byte) 0x3f, 0x66
        };

        byte[] inCopy = in;

        for (int i = 0; i < 1000000; i++) {
            sm4Impl.sms4(in, in.length, key, out, ENCRYPT);
            in = out;
        }
        print("明文:");
        printBytesToInt(inCopy);

        print("密钥:");
        printBytesToInt(key);

        print("密文:");
        printBytesToInt(out);

        // 校验加密
        assertEquals(Hex.encode(expectEncrpt), Hex.encode(out));

        for (int i = 0; i < 1000000; i++) {
            sm4Impl.sms4(out, out.length, key, deOut, DECRYPT);
            out = deOut;
        }
        print("解密:");
        printBytesToInt(deOut);

        // 校验解密
        assertEquals(Hex.encode(inCopy), Hex.encode(deOut));

    }

    private static void printBytesToInt(byte[] in) {
        for (int i = 0; i < in.length; i++) {
            System.out.print(Integer.toHexString(in[i] & 0xff) + "\t");
            if ((i + 1) % 16 == 0) {
                System.out.println();
            }
        }
    }

    private static void print(String s) {
        System.out.print(s);
    }
}
